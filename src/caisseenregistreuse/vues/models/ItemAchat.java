/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caisseenregistreuse.vues.models;

import caisseenregistreuse.modeles.Encodable;

/**
 *
 * @author E140928
 */
public class ItemAchat {
    int quantite;
    String etiquette;
    float montant;
    
    public ItemAchat(Encodable item, int quantite){
        this.quantite=quantite;
        this.etiquette=item.getEtiquette();
        this.montant=item.getMontant()*quantite;
    }
    
    
    
    
    
    
    public int getQuantite(){return this.quantite;}
    public String getEtiquette(){return this.etiquette;}
    public float getMontant(){return this.montant;}
    
}
