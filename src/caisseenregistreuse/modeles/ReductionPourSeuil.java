/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caisseenregistreuse.modeles;

/**
 *
 * @author E140928
 */
public class ReductionPourSeuil extends Reduction {
    private float montantLimite;

    public ReductionPourSeuil(String article, float reduction, float montantLimite){
        super(article, reduction);
        this.montantLimite=montantLimite;
    }
    @Override
    public boolean estAjoutableA(ListeAchats liste) {
        if(liste.estDansLaListe(this)) return false;
        if(liste.total()>= this.montantLimite) return true;
        return false;
    }    
}
