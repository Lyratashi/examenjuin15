/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caisseenregistreuse.modeles;

/**
 *
 * @author E140928
 */
public class ReductionPourArticle extends Reduction {
        private final Article reduction;
        private final int minimumRequis;
        public ReductionPourArticle(String etiquette, Article reduction, int requis) {
        super(etiquette, reduction.getMontant());
        this.reduction= reduction;
        this.minimumRequis=requis;
    }
    public Article getReduction(){return this.reduction;}
    @Override public boolean estAjoutableA(ListeAchats liste) {
        int quantite = liste.quantiteDe(this);
        if(quantite > 0) return false;
        quantite = liste.quantiteDe(this.reduction);
        return (quantite >= this.minimumRequis);  
    }
    @Override public void ajouterA(ListeAchats liste){
        liste.ajouterNouveauArticle(this);
        liste.augmenterLaQuantiteDe(this.getReduction());
   }
}
