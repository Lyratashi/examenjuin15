/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caisseenregistreuse.modeles;

import java.util.ArrayList;

/**
 *
 * @author E140928
 */
public class ListeArticles {
    private final ArrayList<Encodable> liste;
    
    
    public ListeArticles(){
        this.liste = new ArrayList();
        this.liste.add(new Article("Déodorant spécial laboratoire", 4.5f));
        this.liste.add(new Article("Gomina pour cheveux secs", 5.5f));
        this.liste.add(new Article("Parfum le 'geek' by Coco Chanel", 20.5f));
        this.liste.add(new Article("Netoyant pour lunettes", 5.0f));
        this.liste.add(new Article("Couple-ongle 'sweet keyboard'", 10.5f));
        this.liste.add(new ReductionPourSeuil("Réduction de 5 euros sur un montant total >=25 ", 5.0f, 25.0f));
        this.liste.add(new ReductionPourArticle("Réduction 4 Déodorant achetes 1 offert", (Article)this.liste.get(0), 4));
        this.liste.add(new ReductionPourArticle("Reduction 2 gomina achetes 1 offerte", (Article)this.liste.get(1), 1));
    }
    public ArrayList<Encodable> getListeArticles(){return this.liste;}
    public Encodable getElement(int i){return this.liste.get(i);}
    public Encodable getElement(Encodable item){
        return this.getElement(this.getListeArticles().indexOf(item));
    }
}
