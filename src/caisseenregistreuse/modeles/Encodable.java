package caisseenregistreuse.modeles;

/**
 * Cette classe est à compléter.
 */
public abstract class Encodable {
   private final String ETIQUETTE;
   
   public Encodable(String article){this.ETIQUETTE= article;}
   public String getEtiquette(){return this.ETIQUETTE;}
   public abstract boolean estAjoutableA(ListeAchats liste);
   public abstract float getMontant();
   public void ajouterA(ListeAchats liste){
     if(liste.quantiteDe(this)>0) liste.augmenterLaQuantiteDe(this);
     else liste.ajouterNouveauArticle(this);
   }
}
