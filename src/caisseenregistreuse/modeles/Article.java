/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caisseenregistreuse.modeles;

/**
 *
 * @author E140928
 */
public class Article extends Encodable {
    private final float prixArticle;

    public Article(String etiquette, float montant){
        super(etiquette);
        this.prixArticle= montant;
    }
    @Override public boolean estAjoutableA(ListeAchats liste) { return true;}
    @Override public float getMontant() { return this.prixArticle;}
    
    
}
