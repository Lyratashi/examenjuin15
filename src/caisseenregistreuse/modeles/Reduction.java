/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caisseenregistreuse.modeles;

/**
 *
 * @author Lz
 */
public abstract class Reduction extends Encodable{
   private float reduction; 
    
   public Reduction(String etiquette, float reduction){
       super(etiquette);
       this.reduction=-Math.abs(reduction);
   }
   @Override public abstract boolean estAjoutableA(ListeAchats liste);
    @Override public final float getMontant(){return this.reduction;}
    
}
