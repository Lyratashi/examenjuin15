/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caisseenregistreuse.modeles;

import caisseenregistreuse.controleurs.IArticles;
import caisseenregistreuse.vues.models.ItemAchat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author E140928
 */
public class ListeAchats implements IArticles{
    private HashMap achats;
    private float montantTotal;
    
    public ListeAchats(){
        this.achats=new HashMap();
        this.montantTotal= 0.0f;
    }
        
    public ListeAchats(Encodable items, int quantite){
        this();
        this.achats.put(items, quantite);
    }
    public void ajouter(Encodable items){
        if(items.estAjoutableA(this)) items.ajouterA(this);;
    }
    public int quantiteDe(Encodable items){
        if(!this.estDansLaListe(items)) return 0;
        return (int)this.achats.get(items);
    }
    public float total(){return this.montantTotal;}
    public boolean estDansLaListe(Encodable items){return this.achats.containsKey(items);}
    public ArrayList<ItemAchat> getAchat(){
        ArrayList<ItemAchat> liste = new ArrayList();
        for(Encodable items: LISTE_ARTICLES.getListeArticles()){
            if(this.estDansLaListe(items)) liste.add(new ItemAchat(items, (int)this.achats.get(items)));
        }
        return liste;   
    }
    public void clear(){
        this.achats.clear();
        this.montantTotal = 0.0f;
    }
    public void augmenterLaQuantiteDe(Encodable items){
        this.achats.replace(items, this.quantiteDe(items)+1);
        this.ajouterAuTotal(items);
    }
    public void ajouterNouveauArticle(Encodable items){
        this.achats.put(items, 1);
        this.ajouterAuTotal(items);   
    }
    private void ajouterAuTotal(Encodable items){this.montantTotal+=items.getMontant();}
    public HashMap getListe(){return this.achats;}
    
}
