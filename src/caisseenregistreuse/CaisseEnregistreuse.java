package caisseenregistreuse;

import helmo.nhpack.NHPack;

public class CaisseEnregistreuse {
    public static void main(String[] args) {
        NHPack.getInstance().loadMainWindow("caisseenregistreuse.vues.EncodageAchat.xml");
        NHPack.getInstance().showMainWindow();
    }
    
}
