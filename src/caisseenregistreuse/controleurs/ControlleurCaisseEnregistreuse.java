/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caisseenregistreuse.controleurs;

import caisseenregistreuse.modeles.ListeAchats;
import helmo.nhpack.NHPack;

/**
 *
 * @author E140928
 *
 * Classe pauvre je n ai pas eu assez de temps il manque l affichage du montant
 */
public class ControlleurCaisseEnregistreuse {

    float payement;
    ListeAchats liste;

    public ControlleurCaisseEnregistreuse() {
        this.payement = 0.0f;
    }

    public ControlleurCaisseEnregistreuse(ListeAchats liste) {
        this();
        this.liste = liste;
    }

    public float getPayement() {
        return this.payement;
    }

    public void setPayement(float payement) {
        this.payement = payement;
    }

    public void payer() {
        if (payementEstValide()) {
            float reste = this.payement - this.liste.total();
            NHPack.getInstance().showInformation("OK", String.format("Le reste %.2f", reste));
            this.liste.clear();
            NHPack.getInstance().closeWindow("caisseenregistreuse.vues.CaisseEnregistreuse.xml");
            NHPack.getInstance().refreshMainWindow();
        } else {
            NHPack.getInstance().showError("ERROR", "Payement Impossible");
        }
     
    }

    public boolean payementEstValide() {
        return this.payement >= liste.total();
    }
    public String getAPayer(){
        return String.format("Montant : (A payer: %.2f)",this.liste.total());
                
    }
            
}
