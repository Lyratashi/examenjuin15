package caisseenregistreuse.controleurs;

import caisseenregistreuse.CaisseEnregistreuse;
import caisseenregistreuse.modeles.Encodable;
import caisseenregistreuse.modeles.ListeAchats;
import caisseenregistreuse.vues.models.ItemAchat;
import helmo.nhpack.NHPack;
import java.util.*;

/**
 * Les objets de cette classe controlent la fenêtre d'encodage.
 */
public class ControleurEncodageAchat implements IArticles {
    private ListeAchats mesAchats;
    private Encodable item;
    
    public ControleurEncodageAchat(){
        this.mesAchats=new ListeAchats();
        this.item=LISTE_ARTICLES.getElement(0);
    }

    /** Méthode appelée par le nhpack pour obtenir la liste des items
     *  qu'un utilisateur peut encoder.
     */
    public List<Encodable> getItems() {
        return LISTE_ARTICLES.getListeArticles();
    }

    /**
     * Méthode appelée par le nhpack pour afficher l'item sélectionné dans la liste.
     */
    public Encodable getItemSelectionne() {
        return item;
       
    }

    /**
     * Méthode pour appelée par le nhpack pour indiquer au controleur l'item 
     * sélectionné par l'utilisateur.
     */
    public void setItemSelectionne(Encodable item) {
        this.item=item;
    }

    /**
     * Action déclenchée par la fenêtre d'encodage lorsque l'utilisateur
     * clique sur le bouton Ajouter.
     */
    public void ajouter() {
       this.mesAchats.ajouter(LISTE_ARTICLES.getElement(this.item));
    }

    /**
     * Méthode appelée pour obtenir la liste des achats de l'utilisateur.
     */
    public List<ItemAchat> getAchats() {
        return mesAchats.getAchat();
    }

    /**
     * Méthode appelée par le nhpack pour obtenir le montant total 
     * de la liste d'achats.
     */
    public String getTotal() {
        return this.mesAchats.total()+" euros";
    }

    /**
     * Méthode appelée lorsque l'utilisateur clique sur le bouton "Payer".
     */
    public void lancerPaiement() {
        ControlleurCaisseEnregistreuse MaCaisse = new ControlleurCaisseEnregistreuse(this.mesAchats);
        NHPack.getInstance().loadWindow("caisseenregistreuse.vues.CaisseEnregistreuse.xml", MaCaisse);
        NHPack.getInstance().showWindow("caisseenregistreuse.vues.CaisseEnregistreuse.xml");
    }

}
